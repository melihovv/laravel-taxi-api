<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Driver;
use App\Models\Operator;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

class InitController extends Controller
{
    public function index()
    {
        DB::statement('SET foreign_key_checks=0');
        Order::truncate();
        Car::truncate();
        Driver::truncate();
        DB::statement('TRUNCATE TABLE car_driver;');
        Operator::truncate();
        DB::statement('SET foreign_key_checks=1');

        $drivers = factory(Driver::class, 5)->create();
        $cars = factory(Car::class, 10)->create();
        $operators = factory(Operator::class, 3)->create();

        foreach ($drivers->take(4) as $driver) {
            $driver->cars()->attach($cars->pluck('id')->random(2));
        }

        foreach (range(1, 27) as $i) {
            $driver = $drivers->random();

            if ($driver->cars->isEmpty()) {
                continue;
            }

            factory(Order::class)->create([
                'driver_id' => $driver->id,
                'car_id' => $driver->cars->random()->id,
                'operator_id' => $operators->random()->id,
            ]);
        }

        foreach (range(1, 3) as $i) {
            $driver = $drivers->random();

            factory(Order::class)->create([
                'driver_id' => $driver->id,
                'car_id' => $driver->cars->random()->id,
                'operator_id' => $operators->random()->id,
                'order_date_start' => now(),
            ]);
        }

        $driversWithoutOrders = factory(Driver::class, 2)->create();

        factory(Order::class, 10)->create([
            'driver_id' => $drivers->first()->id,
            'car_id' => $drivers->first()->cars->random()->id,
            'operator_id' => $operators->random()->id,
        ]);

        return response()->json(['message' => 'Ok']);
    }
}
