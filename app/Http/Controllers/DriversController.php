<?php

namespace App\Http\Controllers;

use App\Http\Requests\DriversWithGreaterThanXOrdersRequest;
use App\Http\Resources\DriverResource;
use App\Models\Driver;
use Illuminate\Http\Request;

class DriversController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $drivers = Driver::withCount('orders')->orderByDesc('orders_count')->get();

        return DriverResource::collection($drivers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json(['message' => 'Not implemented.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function show(Driver $driver)
    {
        return response()->json(['message' => 'Not implemented.']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Driver $driver)
    {
        return response()->json(['message' => 'Not implemented.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver $driver)
    {
        return response()->json(['message' => 'Not implemented.']);
    }

    /**
     * Get list of drivers, which do not have any order.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function withoutOrders()
    {
        return DriverResource::collection(Driver::doesntHave('orders')->get());
    }

    /**
     * Get list of drivers, which have more than X orders.
     *
     * @param DriversWithGreaterThanXOrdersRequest $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function withGreaterThanXOrders(DriversWithGreaterThanXOrdersRequest $request)
    {
        return DriverResource::collection(Driver::has('orders', '>', $request->amount ?: 100)->get());
    }
}
