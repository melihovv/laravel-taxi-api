<?php

namespace App\Http\Controllers;

use App\Http\Resources\EmployeeResource;
use App\Models\Driver;
use App\Models\Operator;

class EmployeesContoller extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $employees = Driver::all()->toBase()->merge(Operator::all()->toBase());

        return EmployeeResource::collection($employees);
    }
}
