<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderAddressResource;
use App\Models\Order;

class AddressesController extends Controller
{
    /**
     * Get addresses of non successful orders.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function addressesOfNonSuccessfulOrders()
    {
        $orders = Order::with('operator:id,name')
            ->where('success', false)
            ->get(['id', 'address_from', 'address_to', 'operator_id']);

        return OrderAddressResource::collection($orders);
    }
}
