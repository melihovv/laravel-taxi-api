<?php

namespace App\Http\Controllers;

use App\Http\Requests\CarsWithDriversBetweenRequest;
use App\Http\Resources\CarResource;
use App\Models\Car;
use Illuminate\Http\Request;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['message' => 'Not implemented.']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json(['message' => 'Not implemented.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        return response()->json(['message' => 'Not implemented.']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car)
    {
        return response()->json(['message' => 'Not implemented.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        return response()->json(['message' => 'Not implemented.']);
    }

    /**
     * Get list of cars, which are used by more than X drivers and less than Y ones.
     *
     * @param CarsWithDriversBetweenRequest $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function withDriversBetween(CarsWithDriversBetweenRequest $request)
    {
        $cars = Car::has('drivers', '>', $request->from ?: 1)
                ->has('drivers', '<', $request->to ?: 4)
                ->get();

        return CarResource::collection($cars);
    }
}
