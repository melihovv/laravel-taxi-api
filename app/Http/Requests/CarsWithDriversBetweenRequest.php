<?php

namespace App\Http\Requests;

class CarsWithDriversBetweenRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from' => 'nullable|integer|min:1',
            'to' => 'nullable|integer|min:1',
        ];
    }
}
