<?php

namespace App\Http\Requests;

class DriversWithGreaterThanXOrdersRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'nullable|integer|min:1',
        ];
    }
}
