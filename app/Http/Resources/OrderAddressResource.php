<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class OrderAddressResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'address_from' => $this->address_from,
            'address_to' => $this->address_to,
            'operator_name' => $this->operator->name ?? '',
        ];
    }
}
