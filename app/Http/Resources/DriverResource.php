<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class DriverResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'position' => $this->position,
            'orders_count' => $this->when($this->orders_count !== null, $this->orders_count),
        ];
    }
}
