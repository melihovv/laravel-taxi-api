<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CarResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'model' => $this->model,
            'color' => $this->color,
            'number' => $this->number,
        ];
    }
}
