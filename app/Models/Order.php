<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    protected $dates = [
        'order_date_start',
        'order_date_finish',
    ];

    protected $fillable = [
        'driver_id',
        'car_id',
        'operator_id',
        'address_from',
        'address_to',
        'client_arrived',
        'success',
        'order_date_start',
        'order_date_finish',
    ];

    protected $casts = [
        'driver_id' => 'integer',
        'car_id' => 'integer',
        'operator_id' => 'integer',
        'client_arrived' => 'boolean',
        'success' => 'boolean',
    ];

    /**
     * @return BelongsTo
     */
    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    /**
     * @return BelongsTo
     */
    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    /**
     * @return BelongsTo
     */
    public function operator()
    {
        return $this->belongsTo(Operator::class);
    }
}
