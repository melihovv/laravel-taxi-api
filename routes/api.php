<?php

use Illuminate\Support\Facades\Route;

Route::get('init', 'InitController@index')->name('init');

Route::get('addresses/of-non-successful-orders', 'AddressesController@addressesOfNonSuccessfulOrders');

Route::get('cars/with-drivers-between', 'CarsController@withDriversBetween');

Route::get('employees', 'EmployeesContoller@index');

Route::get('drivers/without-orders', 'DriversController@withoutOrders');
Route::get('drivers/with-greater-than-x-orders', 'DriversController@withGreaterThanXOrders');

Route::apiResources([
    'drivers' => 'DriversController',
    'operators' => 'OperatorsController',
    'cars' => 'CarsController',
    'orders' => 'OrdersController',
]);
