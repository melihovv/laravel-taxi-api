# Requirements
- php >= 7.0
- composer

# Install
- git clone https://bitbucket.org/melihovv/laravel-taxi-api
- cd laravel-taxi-api
- composer install
- cp .env.example .env
- php artisan key:generate
- specify DB_DATABASE, DB_USERNAME, DB_PASSWORD variables to .env file
- php artisan migrate
- php artisan serve

# Endpoints

- `http://localhost:8000/api/init` - Скрипт должен удалять все таблицы из БД, если такие имеются и создавать все необходимые таблицы, после чего внести в них тестовые данные: не менее 5 водителей, не менее 10 автомобилей, не менее 30 записей в истории заказов, не менее 3х заказов “в процессе”..
- `http://localhost:8000/api/drivers/without-orders` - Найти всех водителей, которые не имеют ни одного заказа за всё время работы (минимум 2 в ответе).
- `http://localhost:8000/api/addresses/of-non-successful-orders` - Найти все адреса, которые не имеют успешного статуса и имена операторов, которые их приняли (минимум 5 записей в ответе, операторы могут повторяться).
- `http://localhost:8000/api/drivers/with-greater-than-x-orders` - Найти всех водителей, которые имеют более 100 выполненных заказов (“Not Found”).
- `http://localhost:8000/api/drivers/with-greater-than-x-orders?amount=10` - Найти всех водителей, которые имеют более 10 выполненных заказов (минимум 1 водитель).
- `http://localhost:8000/api/drivers` - Вывести список водителей в порядке убывания количества выполненных заказов (имена всех водителей).
- `http://localhost:8000/api/cars/with-drivers-between` - Найти список автомобилей, на которых работают более 1го водителя и менее 4х водителей (минимум 2 варианта)
- `http://localhost:8000/api/employees` - Список всех сотрудников службы такси (водителей и операторов)

# Main classes/files
- App
    - Models
        - Driver
        - Operator
        - Car
        - Order
    - Http
        - Controllers
            - AddressesController
            - CarsController
            - DriversController
            - EmployeesController
            - InitController
            - OperatorsController
            - OrdersController
        - Resources
            - CarResource
            - DriverResource
            - EmployeeResource
            - OrderAddressResource
- database
    - factories
        - ModelFactories.php
    - migrations
        - create_*_table.php
- routes/api.php

