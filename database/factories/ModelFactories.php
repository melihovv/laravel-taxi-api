<?php

use App\Models\Car;
use App\Models\Driver;
use App\Models\Operator;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/**
 * @var Factory $factory
 */

$factory->define(App\Models\Car::class, function (Faker $faker) {
    return [
        'model' => $faker->randomElement(['Ford', 'Lexus', 'Toyota', 'Fiat', 'Ferrari']),
        'color' => $faker->colorName,
        'number' => $faker->randomLetter . $faker->randomNumber(3) . $faker->randomLetter . $faker->randomLetter . $faker->randomNumber(2),
    ];
});

$factory->define(App\Models\Driver::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'position' => $faker->words(3, true),
    ];
});

$factory->define(App\Models\Operator::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'position' => $faker->words(3, true),
    ];
});

$factory->define(App\Models\Order::class, function (Faker $faker) {
    return [
        'driver_id' => function () {
            return factory(Driver::class)->create()->id;
        },
        'car_id' => function () {
            return factory(Car::class)->create()->id;
        },
        'operator_id' => function () {
            return factory(Operator::class)->create()->id;
        },
        'address_from' => $faker->address,
        'address_to' => $faker->address,
        'client_arrived' => $faker->boolean(),
        'success' => $faker->boolean(),
        'order_date_start' => $start = now()->subDay($faker->randomNumber(1)),
        'order_date_finish' => (clone $start)->addMinutes($faker->randomNumber(2)),
    ];
});
