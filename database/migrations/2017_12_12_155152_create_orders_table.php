<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('driver_id');
            $table->unsignedInteger('car_id');
            $table->unsignedInteger('operator_id');
            $table->string('address_from');
            $table->string('address_to');
            $table->boolean('client_arrived');
            $table->boolean('success');
            $table->dateTime('order_date_start');
            $table->dateTime('order_date_finish')->nullable();
            $table->timestamps();

            $table
                ->foreign('driver_id')
                ->references('id')
                ->on('drivers')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table
                ->foreign('car_id')
                ->references('id')
                ->on('cars')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table
                ->foreign('operator_id')
                ->references('id')
                ->on('operators')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
